#programme corriger

annee = input("Saisissez une année : ")
annee = int(annee)
bissextile = False


if annee % 400 == 0:
   bissextile = True
elif annee % 100 == 0:
   bissextile = False
elif annee % 4 == 0:
   bissextile = True
else:
   bissextile = False

if bissextile:
   print("L'année saisie est bissextile.")
else:
   print("L'année saisie n'est pas bissextile.")

#programme avec les paranthèse:

Programme testant si une année, saisie par l'utilisateur, est bissextile ou non

annee = input("Saisissez une année : ")
annee = int(annee)

if annee % 400 == 0 or (annee % 4 == 0 and annee % 100 != 0):
   print("L'année saisie est bissextile.")
else:
   print("L'année saisie n'est pas bissextile.")

#mon Programme

annee = input("Saisissez une année : ")
annee = int(annee)

if annee % 4 == 0:
    if annee % 400 == 0:
        if annee % 100 == 0:
            print("L'année saisie n'est pas bissextile.")
        else:
            print("L'année saisie est bissextile.")
    else:
        print("L'année saisie n'est pas bissextile.")
else:
    print("L'année saisie n'est pas bissextile.")

#autre version

def is_atypical_leap_year(year):
    return year % 400 == 0

def is_atypical_common_year(year):
    return year % 100 == 0

def is_typical_leap_year(year):
    return year % 4 == 0

def is_leap_year(year):
    if is_atypical_leap_year(year):
        return True
    if is_atypical_common_year(year):
        return False
    return is_typical_leap_year(year)

year = input("Saisissez une année : ")
year = int(year)

if is_leap_year(year):
   print("L'année saisie est bissextile.")
else:
   print("L'année saisie n'est pas bissextile.")
