# -*- coding: utf-8 -*-


class Aventure(object):
    def __init__(self, situations):
        self.situations = situations

    def jouer(self):
        situation = self.donne_situation_demarrage()
        while situation != None:
            nouvelle_situation = situation.execute()
            situation = self.chercher_situation(nouvelle_situation)

    def donne_situation_demarrage(self):
        return self.situations[0]

    def chercher_situation(self, reference):
        for situation in self.situations:
            if reference == situation.reference:
                return situation
        print "Situation inconnue :", reference
        return None


class Situation(object):
    def __init__(self, reference, description, choix):
        self.reference = reference
        self.description = description
        self.choix = choix

    def execute(self):
        print "---"
        print self.description
        if len(self.choix) == 0:
            return ""
        if len(self.choix) == 1:
            return self.choix[0].reference_nouvelle_situation
        print "Vous pouvez :"
        for index, choix in enumerate(self.choix):
            print "\t", index+1, ":", choix.description
        choix = int(input("> "))
        return self.choix[choix-1].reference_nouvelle_situation


class Choix(object):
    def __init__(self, description, reference_nouvelle_situation):
        self.description = description
        self.reference_nouvelle_situation = reference_nouvelle_situation
