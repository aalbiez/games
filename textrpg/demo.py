# -*- coding: utf-8 -*-
from aventure import Aventure, Situation, Choix


example = Aventure([
    Situation(
        reference="1",
        description="En ce moment vous etes en bas de l'escalier. Vous tenez d'une main le gourdin et de l'autre la lampe a huile.",
        choix=[
            Choix(description="vous precipitez dans les escalier", reference_nouvelle_situation="3"),
            Choix(description="monter prudament", reference_nouvelle_situation="7"),
        ]),

    Situation(
        reference="2",
        description="Vous vous battez contre un our feroce mais vous en êtes arriver a bout vous vous retirer 1 dés de degat de la barre de vie.",
        choix=[
            Choix(description="", reference_nouvelle_situation="4"),
        ]),

    Situation(
        reference="3",
        description="Vous avez rompu le fil tendu en travers de la derniere marche.",
        choix=[
            Choix(description="", reference_nouvelle_situation="5"),
        ]),

    Situation(
        reference="4",
        description="Vous pouvez maintenant tranquillement examiner le socle sur lequel se trouvait l'ours. Vous remarquez bientot qu'on peut en soulever le couvercle. Dessous se trouve le parchmin tant recherché. Felicitation.",
        choix=[]),

    Situation(
        reference="5",
        description="Aussitôt que vous avez posé le pied sur le palier, une trappe s'ouvre et vous tombez. Jetez 1 dé et soustrayez le résultat à vos POINTS DE VIE (30 POINTS DE VIE). Ensuite, vous pouvez monter de nouveau l'escalier.",
        choix=[
            Choix(description="", reference_nouvelle_situation="13"),
        ]),

    Situation(
        reference="6",
        description="Il attaque le premier. Menez le combat selon les règles. Chaque fois qu'il attaque avec succès et que votre Parade échoue, vous jetez 2 dés à 6 faces. Pour obtenir vos points de Blessures, il faut que vous déduisiez 4 points des Points d'Impact de l'Ours. Celui-ci doit jeter un dé une fois avant chaque phase du combat. S'il obtient 6, il se dissout sur-le-champ en fumée brune. Vous devez cette victoire au bouclier magique.",
        choix=[
            Choix(description="", reference_nouvelle_situation="4"),
        ]),

    Situation(
        reference="7",
        description="Vous remarquez qu'un fil de fer très fin est tendu à la hauteur d'une main au-dessus de la dernière marche.",
        choix=[
            Choix(description="couper le fil avec précaution", reference_nouvelle_situation="9"),
            Choix(description="l'enjamber", reference_nouvelle_situation="17"),
        ]),

    Situation(
        reference="8",
        description="Subitement l'Ours s'anime et vous attaque. Il grogne de plus en plus fort. Si vous avez trouvé un bouclier pendant l'aventure, rendez-vous au 6, si vous n'avez que votre arme, rendez-vous au 2.",
        choix=[
            Choix(description="", reference_nouvelle_situation="2"),
        ]),

    Situation(
        reference="9",
        description="Vous entendez un léger bruit, comme si un mécanisme caché venait de se déclencher.",
        choix=[
            Choix(description="poser le pied sur le palier", reference_nouvelle_situation="5"),
            Choix(description="d'abord frapper sur le plancher avec un instrument lourd", reference_nouvelle_situation="11"),
        ]),

    Situation(
        reference="10",
        description="Vous avez à peine touché le socle que vous entendez un grognement profond. Maintenant vous pouvez continuer la recherche avec persévérance (rendez- vous au 8), ou l'abandonner. Dans ce cas il faut que vous descendiez voir la vieille dame pour lui avouer votre échec (rendez-vous au 57).",
        choix=[
            Choix(description="continuer la recherche avec persévérance", reference_nouvelle_situation="8"),
            Choix(description="abandonner la recherche. Dans ce cas il faut que vous descendiez voir la vieille dame pour lui avouer votre échec.", reference_nouvelle_situation="57"),
        ]),

    Situation(
        reference="11",
        description="Sur le sol du palier est dissimulée une trappe qui s'ouvre brusquement. Si vous aviez marché dessus, vous seriez tombé.",
        choix=[
            Choix(description="", reference_nouvelle_situation="13"),
        ]),

    Situation(
        reference="12",
        description="La recherche reste infructueuse. Vous déroulez d'innombrables cartes sans trouver de parchemin intéressant. Il ne vous reste que le socle sur lequel se trouve l'Ours.",
        choix=[
            Choix(description="tâter le socle avec précauction", reference_nouvelle_situation="10"),
            Choix(description="abandonner la recherche. Dans ce cas il faut que vous descendiez voir la vieille dame pour lui avouer votre échec.", reference_nouvelle_situation="57"),
        ]),

    Situation(
        reference="13",
        description="Sur le palier se trouve un trou infranchissable. Vous ne pouvez pas accéder aux différentes pièces. Je vous propose de retourner voir la vieille femme et de lui demander de l'aide.",
        choix=[
            Choix(description="", reference_nouvelle_situation="15"),
        ]),

    Situation(
        reference="14",
        description="Vous êtes dans la piece D, la piece secret du captaine barbe-grise y a ses carte marines, incompréhensibles pour un paysan. L'objet le plus remarquable est une immense ours empaillé, a la peau brillante et aux yeux étincelants. Il est debout, et vous menace avec avec ses pattes de devant. L'animal se trouve sur un socle carré et bas, sur lequel sont griffonnés les mots suivants: NE M'ENERVEZ PAS!!",
        choix=[
            Choix(description="examiner la piece de fond en comble en evitant de toucher l'ours", reference_nouvelle_situation="12"),
            Choix(description="le frapper avec voter arme", reference_nouvelle_situation="8"),
        ]),

    Situation(
        reference="15",
        description="La vieille femme vous regarde avec désapprobation et dit : « Ça commence bien, mon jeune ami ! » Elle fronce les sourcils. « Je ne sais même pas, dit-elle, si je dois vous aider. Celui qui ne sait pas se débrouiller seul ne devrait même pas pénétrer dans la pièce. Enfin, pour cette fois, je veux bien... » Elle croise les bras, ferme les yeux et baisse violemment la tête : « La trappe s'est refermée. Vous pouvez monter sur le palier sans danger. Si je peux vous aider une deuxième fois, je ne le sais pas encore. Réfléchissez donc bien avant de revenir me voir! ». Maintenant vous pouvez remonter l'escalier",
        choix=[
            Choix(description="", reference_nouvelle_situation="17"),
        ]),

    Situation(
        reference="16",
        description="A peine avez-vous dit Sésame, ouvre-toi !, que les boiseries s'écartent et ouvrent le passage vers la pièce d'à côté. Franchissez l'ouverture.",
        choix=[
            Choix(description="", reference_nouvelle_situation="14"),
        ]),

    Situation(
        reference="17",
        description="Vous êtes sur le palier (2 m x 1 m). Trois portes mènent aux chambres. Vous pouvez essayer de les ouvrir.",
        choix=[
            Choix(description="vous tournez vers la porte est", reference_nouvelle_situation="19"),
            Choix(description="vous tournez vers la porte ouest", reference_nouvelle_situation="51"),
            Choix(description="vous tournez vers la porte nord", reference_nouvelle_situation="29"),
        ]),

    Situation(
        reference="18",
        description="Pour ouvrir la porte secrète, il faut connaître le mot de passe que le Capitaine a noté sur un papier.",
        choix=[
            Choix(description="vous connaissez le mot de passe", reference_nouvelle_situation="16"),
            Choix(description="vous ne connaissez pas le mot de passe", reference_nouvelle_situation="56"),
        ]),

    Situation(
        reference="19",
        description="La porte est massive et blindée. Elle est fermée, et il n 'y a pas de clé dans la serrure. Si vous en possédez une, et si vous voulez l'essayer, rendez-vous au 36. Si vous voulez demander la clé à la vieille femme, descendez la voir (rendez-vous au 21). Si vous n'avez pas de clé, vous devez forcément vous tourner vers les portes ouest (rendez-vous au 51) ou nord (rendez-vous au 29), car il est impossible de forcer la porte est.",
        choix=[
        ]),

    Situation(
        reference="20",
        description="""Le Démon tombe à terre et se transforme en un brouillard verdâtre qui se dirige vers le cadre du miroir. Les débris du miroir s'évaporent eux aussi et se reconstituent dans le cadre. Bientôt le miroir a repris sa forme antérieure : totalement intact, il semble simplement un peu plus terne. Sur le verre apparaît une inscription :

        « Félicitations ! Tu es un combattant téméraire. Pour ta récompense voici un message : le parchemin se trouve dans la pièce derrière la porte secrète de la chambre à coucher du Capitaine (pièce C). Il y a aussi, sur un socle, un Ours qui n'est pas empaillé mais enchanté. Tu dois le blesser avec ton arme. Certes, il se défendra, mais tu ne peux pas obtenir le parchemin d'une autre façon, car il est caché dans le socle. Bonne chance et adieu ! »

        L'inscription sur le miroir s'évanouit. Vous pouvez maintenant quitter la pièce (rendez-vous au 17), boire le contenu de la bouteille dorée, si vous ne l'avez pas encore fait ! (rendez-vous au 30) ou inspecter soigneusement la chambre de Barbe-Grise (rendez-vous au 32).""",
        choix=[
        ]),

    Situation(
        reference="21",
        description="""Si vous demandez de l'aide pour la première fois à la vieille femme dans cette aventure, rendez-vous au 27. Si c'est la deuxième ou la troisième fois, rendez- vous au 23.""",
        choix=[
        ]),

    Situation(
        reference="22",
        description="""A peine avez vous touché le miroir qu'il se brise, et qu'un Démon apparaît. Dans la main droite il tient une arme semblable à la vôtre. Il vous semble que les expressions de son visage s'identifient aux vôtres : « Tu as raison, murmure cette étrange créature, je suis ton double. Mais cela ne signifie pas du tout que je vais t'aider. Bien au contraire ! J'essayerai de te tuer, car c'est mon devoir. Je suis le seul à savoir comment tu peux arriver jusqu'au parchemin. Cependant je ne parlerai que lorsque tu m'auras vaincu. » 26Vous luttez maintenant contre le Démon. Il possède les mêmes valeurs de combat que vous (Attaque : 11, Parade : 8). Si vous êtes armé d'un gourdin, le Démon se bat avec 1D+ 2. Si vous avez un sabre d’abordage, le Démon a le même (1 D + 3). Le Démon attaque le premier. Vous possédez cependant le bouclier magique; ce qui signifie que chaque fois que le Démon vous touche et gagne des Points d'Impact, vous en parez automatiquement 4 avec votre bouclier. D'autre part, le Démon doit jeter le dé avant chaque combat. S'il obtient 6, il tombe abattu sur place. (Le Démon a autant de POINTS DE VIE que vous à ce moment de l'aventure.) Si, durant le combat, vos POINTS DE VIE descendent jusqu'à 0, vous êtes mort (rendez-vous au 41). Si vous tuez le Démon, rendez-vous au 20.""",
        choix=[
        ]),

    Situation(
        reference="23",
        description="""La vieille femme se fâche : « J'aurais attendu plus de vous, crie-t-elle. Si j'avais possédé la clé, je vous l'aurais bien entendu donnée ! ».""",
        choix=[
            Choix(description="", reference_nouvelle_situation="25"),
        ]),

    Situation(
        reference="24",
        description="""A peine avez-vous touché le miroir, qu'il se brise. Un Démon apparaît, se faufile, prend un bouclier au mur, et le tient dans sa main gauche. Dans sa main droite, la créature tient une arme, la même que la vôtre. Il vous semble que les expressions de son visage s'identifient aux vôtres : « Tu as raison, murmure cette étrange créature, je suis ton double. Je vais te tuer maintenant, car c'est mon devoir. Pourtant je suis le seul à savoir comment tu peux arriver jusqu'au parchemin. Mais je ne parlerai que lorsque tu m'auras vaincu. » Menez le combat selon les règles qui vous sont déjà connues. Le Démon possède les mêmes valeurs de combat que vous (Attaque : 11, Parade : 8). Si vous possédez un gourdin (1 D + 2), le Démon jette 1 dé et ajoute également 2. Si vous êtes en possession d'un sabre d'abordage (1 D + 3), le Démon a le même. Malheureusement il possède un bouclier, vous pas ! Pour le combat cela signifie que, si vous touchez le Démon, il pare jusqu'à 4 Points d'Impact avec son bouclier. Seuls les Points d'Impact au-delà de 4 seront retirés de son Energie Vitale. Le Démon possède autant de POINTS DE VIE que vous en ce moment. Exemple de combat : Vous jetez le dé pour l'Attaque : 9. Un très beau jet ! La Parade du démon échoue. Maintenant vous jetez le dé pour obtenir les Points d'Impact de votre arme. Supposons que le résultat total soit 7, desquels 4 sont annulés par le bouclier magique du Démon. On retire donc que 3 POINTS DE VIE au Démon. Si vous avez vaincu le Démon, rendez-vous au 20, si le Démon vous tue, rendez-vous au 41.""",
        choix=[
        ]),

    Situation(
        reference="25",
        description="""« Vous n'êtes pas à la hauteur ! continue la vieille femme. Abandonnez ! Allez-vous-en et envoyez-moi un vrai Aventurier ! »

        Vous avez entendu ce que la vieille femme vient de dire : partez et revenez dans la peau d'un autre. Trouvez-vous un nouveau nom, armez cet Aventurier des valeurs d'Alric, et plongez de nouveau dans l'aventure. Cela signifie que vous commencez au 1. Toutes les portes sont refermées, les trésors à leur place, et les pièges installés. Ce n'est pas la peine que le nouvel Aventurier joue l'innocent. Il peut naturellement faire appel aux expériences de son prédécesseur. Nous supposerons qu'Alric lui a raconté ses aventure dans la maison de Barbe-Grise ...""",
        choix=[
        ]),

    Situation(
        reference="26",
        description="""Si vous avez pris le bouclier avant de nettoyer le miroir, rendez-vous au 22. Si le bouclier est toujours accroché au mur rendez-vous au 24.""",
        choix=[
        ]),

    Situation(
        reference="27",
        description="""La vieille femme ne comprend pas votre requête : « Si j'avais la clé, dit-elle, je vous l'aurais donnée. Remontez et cherchez-la ! D'autre part je vous prie de ne plus me déranger qu'en cas d'extrême urgence. En véritable Aventurier, vous devez agir seul, ou alors vous n'irez pas bien loin! » Retournez sur le palier au 17 !""",
        choix=[
        ]),

    Situation(
        reference="28",
        description="""Lorsque vous portez le bouclier à votre bras gauche vous sentez qu'il s'agit d'un objet magique. Intuitivement vous comprenez tout de suite quelle puissance se cache en lui. La tête de mort rouge, sur bouclier, est un puissant signe Vaudou. Un adversaire qui la regarde trop longtemps tombe mort. (Lors d'un combat ultérieur, si vous portez le bouclier, votre adversaire devra, avant chaque assaut, lancer une fois le dé à 6 faces. S'il obtient 6, il meurt subitement.) D'autre part le bouclier magique vous protège des coups de votre adversaire. S'il vous touche, 4 de ses Points d'Impact sont absorbés par le bouclier. Exemple : Votre adversaire réussit son Attaque, tandis que votre Parade est inefficace. Votre adversaire joue au dé les Points d'Impact de son arme. Supposons qu'il possède un sabre d'abordage (1 D + 3). Il obtient 4, soit un total de 7. Normalement ces 7 points devraient être retirés de votre Energie Vitale, mais 4 de ces points seront arrêtés par le bouclier. Vous soustrayez donc seulement 3 points de vos POINTS DE VIE. Maintenant vous pouvez boire le contenu de la petite bouteille, si vous ne l'avez pas encore fait (rendez- vous au 30), nettoyer le miroir (rendez-vous au 26), poursuivre la fouille de la petite pièce (rendez- vous au 32) ou la quitter (rendez-vous au 17).""",
        choix=[
        ]),



])

example.jouer()
