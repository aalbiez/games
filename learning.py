#!/bin/python
# -*- coding: utf-8 -*-

def head(list):
    """ Return the first element of a list """
    return list[0]

def tail(list):
    """ Return the list without the first element """
    return list[1:]

def is_empty(list):
    """ Test if the list is empty """
    return list == []


def contains_iteratif(list, value):
    """ Test if the list contains the value """
    for element in list:
        if value == element:
            return True
    return False


def contains(list, value):
    """ Test if the list contains the value """
    print("Est-ce que " + repr(list) + " contient " + repr(value))
    if is_empty(list):
        print "La liste est vide, donc non"
        return False
    print "Je teste si le premier element est le bon"
    if head(list) == value:
        print "Oui, donc on a trouvé"
        return True
    print "Non, donc je me rapelle avec ce qu'il reste de la liste"
    return contains(tail(list), value)


def fact_iterative(n):
    result = 1
    while n > 0:
        result = result * n
        n = n - 1
    return result


def fact_recursive(n):
    if n == 1:
        return 1
    return fact_recursive(n-1) * n



print(fact_recursive(3), " should be 6")

print "="*80
print(contains([1, 4, 7, 8], 4), " should be True")
print "="*80
print(contains([1, 4, 7, 8], 5), " should be False")
print "="*80
