# -*- coding: utf-8 -*-
from math import sqrt


class node(object):
    def __init__ (self, operator, left, right):
        self.operator = operator
        self.left = left
        self.right = right

    def human_render(self):
        return "(" + self.left.human_render() + " " + self.operator + " " + self.right.human_render() + ")"

    def postfix_render(self):
        return self.left.postfix_render() + " " +   self.right.postfix_render() + " " + self.operator

    def compute(self):
        if self.operator == '+':
            return self.left.compute() + self.right.compute()
        elif self.operator == '*':
            return self.left.compute() * self.right.compute()
        elif self.operator == '-':
            return self.left.compute() - self.right.compute()

    def rasincarre(self):
        resultat = self.compute()
        return sqrt(resultat)



class leaf(object):
    def __init__ (self, valeur):
        self.valeur = valeur

    def human_render(self):
        return str(self.valeur)

    def postfix_render(self):
        return str(self.valeur)

    def compute(self):
        return self.valeur


example = node(
    operator = '+',
    left = leaf(18),
    right = node(
        operator = '*',
        left = node(
            operator = '-',
            left = leaf(3),
            right = leaf(4)),
        right = leaf(10)))

#print(example.human_render())
#print(example.postfix_render())
print(example.compute())
print(example.rasincarre())
