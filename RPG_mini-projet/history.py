# -*- coding: utf-8 -*-
from aventure import Aventure, Situation, Choix
from game import Game
from ui_console import ConsoleUI


example = Aventure([
    Situation(
        reference="0",
        description="Pour cree votre personnage que vous jouerez pendant cette aventure lancez un des de 10, ajoute 10 auresultat et noté sur une fiche le nombre sa sera votre santé, vous parté avec 10 d'attaque et un emplacement d'arme vide.",
        choix=[
            Choix(description="Commencer l'aventure", reference_nouvelle_situation="1")
        ]),

    Situation(
        reference="1",
        description="Apres de nombreuse heure de caval pour fuir ce monstre, vous arrivez dans une salle carre avec des babiolle sur les cotés, au fond il y a une porte.",
        choix=[
            Choix(description="fouiller les babiolle", reference_nouvelle_situation="2"),
            Choix(description="Passez la porte", reference_nouvelle_situation="3")
        ]),

    Situation(
        reference="2",
        description="En fouillant les babiolle vous trouvez une epee ornee de pierre presieuse. A vu, elle semble de qualiter mais vous nette pas sur. vous pouvez la rajouter dans votre inventaire. elle donne un bonus d'attaque +2.",
        choix=[
            Choix(description="prendre la porte", reference_nouvelle_situation="3")
        ]),

    Situation(
        reference="3",
        description="Vous entrez dans un long couloir etroit et sombre avec au fond un orc, derière lui une impossante porte avec au dessus un petit encadre, dessus est ecrit EXIT, il ne vous a pas vue.",
        choix=[
            Choix(description="engager le combat", reference_nouvelle_situation="4"),
            Choix(description="retourner dans la piece precedente", reference_nouvelle_situation="1")
        ]),

    Situation(
        reference="4",
        description="vous engager le combat evec l'orc. lancer un des de 20 si il est inférieur a votre score d'attaque vous toucher en mettant 5 piont de degat sinon vous vous prenez 5piont de degat, l'orc a 20HP et le premièer arriver a 0 meurt.",
        choix=[
            Choix(description="victoire", reference_nouvelle_situation="5"),
            Choix(description="defait", reference_nouvelle_situation="6")
        ]),

    Situation(
        reference="5",
        description="Vous avez térasser l'orc au long d'un dure combat, vous ouvrez la grosse porte qui se trouve derière lui, un grosse lumière vous eblouie. vous voyer des champ d'herbe, vous avez reussie a vous sortire de votre donjon. ",
        choix=[
            Choix(description="", reference_nouvelle_situation="4")
        ]),

    Situation(
        reference="6",
        description="Vous vous ecrouler sous le poid des cou de votre adversaire. vous n'aurrez pas reussi a sortire de se donjon de cauchemar.",
        choix=[
            Choix(description="couper le fil avec précaution", reference_nouvelle_situation="9"),
            Choix(description="l'enjamber", reference_nouvelle_situation="17")
        ]),

])


Game(example, ConsoleUI()).play()
