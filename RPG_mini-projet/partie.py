class Partie(object):
    def __init__(self):
        self.memoire = {}

    def ecrit(self, nom, valeur):
        self.memoire[nom] = valeur

    def lit(self, nom):
        return self.memoire[nom]
