# -*- coding: utf-8 -*-
from ui import UI


class ConsoleUI(UI):
    def display_description(self, description):
        print (description)
        print ("---")

    def display_choices(self, choices):
        print ("Vous pouvez :")
        for index, choice in enumerate(choices):
            print ("\t", index+1, ":", choice.description)

    def select_choice(self, choices):
        while True:
            try:
                selected = int(input("> "))
                if 1 <= selected <= len(choices):
                    return selected
            except ValueError:
                pass
            print("# choix invalide")
