# -*- coding: utf-8 -*-


class Aventure(object):
    def __init__(self, situations):
        self.situations = situations

    def chercher_situation(self, reference):
        for situation in self.situations:
            if reference == situation.reference:
                return situation
        print ("Situation inconnue :", reference)
        return None


class Situation(object):
    def __init__(self, reference, description, choix):
        self.reference = reference
        self.description = description
        self.choix = choix

    def select_choice(self, index):
        if 1 <= index <= len(self.choix):
            return self.choix[index-1].reference_nouvelle_situation
        return None


class Choix(object):
    def __init__(self, description, reference_nouvelle_situation):
        self.description = description
        self.reference_nouvelle_situation = reference_nouvelle_situation
