# -*- coding: utf-8 -*-
from ui import UI


class Game(object):
    def __init__(self, aventure, ui):
        self.aventure = aventure
        self.ui = ui
        self.current_situation_id = "1"

    def play(self):
        situation = self.aventure.chercher_situation(self.current_situation_id)
        while situation != None:
            self.ui.display_description(situation.description)
            self.ui.display_choices(situation.choix)
            index = self.ui.select_choice(situation.choix)
            self.current_situation_id = situation.select_choice(index)
            situation = self.aventure.chercher_situation(self.current_situation_id)
