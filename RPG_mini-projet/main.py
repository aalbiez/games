import pygame
from pygame.locals import *

########################################################################
class Aventure(object):
    def __init__(self, situations):
        self.situations = situations

    def jouer(self):
        situation = self.donne_situation_demarrage()
        while situation != None:
            nouvelle_situation = situation.execute()
            situation = self.chercher_situation(nouvelle_situation)

    def donne_situation_demarrage(self):
        for situation in self.situations:
            if situation.reference == "1":
                return situation
        return self.situations[0]

    def chercher_situation(self, reference):
        for situation in self.situations:
            if reference == situation.reference:
                return situation
        print ("Situation inconnue :", reference)
        return None


class Situation(object):
    def __init__(self, reference, description, choix):
        self.reference = reference
        self.description = description
        self.choix = choix

    def affichage(self):
        pygame.draw.rect(screen, white, pygame.Rect(10, 10, (width-20), (height-80), border_radius = -20))
        pygame.draw.rect(screen, white, pygame.Rect(10, 415, (width-20), (height-425), border_radius = -20))
        font=pygame.font.Font(None, 24)
        text = font.render(self.description,1, black)
        screen.blit(text, (15, 15))
        pygame.display.flip()
        continuer = 1
        while continuer:

            for event in pygame.event.get():
                if event.type == QUIT:
                    continuer = 0


    def execute(self):
        self.affichage()
#        if len(self.choix) == 0:
#            return ""
#        if len(self.choix) == 1:
#            return self.choix[0].reference_nouvelle_situation
#        print ("Vous pouvez :")
#        for index, choix in enumerate(self.choix):
#            print ("\t", index+1, ":", choix.description)
#        while True:
#            try:
#                choix = int(input("> "))
#                if 1 <= choix <= len(self.choix):
#                    return self.choix[choix-1].reference_nouvelle_situation
#            except ValueError:
#                print("# choix invalide")


class Choix(object):
    def __init__(self, description, reference_nouvelle_situation):
        self.description = description
        self.reference_nouvelle_situation = reference_nouvelle_situation
#################################################################################
pygame.init()
width = 640
height = 480
screen = pygame.display.set_mode((width, height))#, pygame.FULLSCREEN
white = 255, 255, 255
black = 0, 0, 0
