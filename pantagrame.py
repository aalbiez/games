import turtle

turtle.bgcolor("black")
t = turtle.Turtle()
t.color("white", "red")
t.begin_fill()
for i in range(0, 5):
    t.forward(200)
    t.right(144)
t.end_fill()

t.ht()
turtle.exitonclick()
