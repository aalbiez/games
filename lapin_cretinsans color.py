from turtle import *


A = [
    None,
    (8, 14),
    (7.2, 17),
    (6, 19),
    (10, 25),
    (10.4, 27),
    (10, 28),
    (8, 29.2),
    (6, 29),
    (4, 27),
    (3, 21),
    (1, 22),
    (-1, 22),
    (-3, 21),
    (-4, 27),
    (-6, 29),
    (-8, 29.2),
    (-10, 28),
    (-10.2, 27),
    (-10, 25),
    (-6, 19),
    (-7.3, 14),
    (-8, 14),
]

B = [
    None,
    (-6,14), #B1
    (-5,13),
    (-5,11),
    (-6,10),
    (-8,10),
    (-9,11),
    (-9,13),
    (-8,13),
    (-6,13),
    (-6,11),
    (-8,11),

    (9, 13), #B12
    (9, 11),
    (8, 10),
    (6, 10), #B15
    (5, 11),
    (5, 13),
    (6, 14),
    (6, 13),
    (8, 13), #B20
    (8, 12),
    (6, 12),
    (9, 12),
    (5, 12), #B24
]

C = [
    None,
    (-6, 9),
    (-4, 8),
    (4, 8),
    (6, 9),
    (7, 7),
    (7, 5),
    (6, 3),
    (5, 2),
    (4, 2),
    (2, 3),
    (-2, 3),
    (-4, 2),
    (-5, 2),
    (-6, 3),
    (-7, 5),
    (-7, 7),

    (-2, 8),
    (-2, 6),
    (-4, 6),
    (1, 8),
    (3, 8),
    (3, 6),
    (1, 6),
]

D = [
    None,
    (-8, 6),
    (-7, 3),
    (-5, 0),
    (-8, -11),
    (-7, -16),
    (-6, -18),
    (-1, -18),
    (0, -17),
    (1, -18),
    (5, -18),
    (7, -16),
    (8, -13),
    (6.3, -5),

    (11, -6.2),
    (12, -8),
    (13, -8),
    (13, -9),
    (14, -9),
    (14, -8),
    (15, -7),
    (15, -5),
    (14, -4),
    (13, -4),
    (11, -5),
    (6, -3),
    (5, 0),
    (7, 3),
    (8, 6),
]

E = [
    None,
    (13, -7),
    (13, -6),
    (12, -4.3),
    (13, -5),
    (14, -5),
    (14, 2),
    (15, 2),
    (15, 3),
    (16, 4),
    (16, 6),
    (11, 6),
    (11, 4),
    (12, 3),
    (12, 2),
    (13, 2),
]

F = [
    None,
    (-6, -3),
    (-11, -10),
    (-13, -12),
    (-13, -14),
    (-11, -15),
    (-9, -14),
    (-9, -13),
    (-11, -13),
    (-11, -12),
    (-10, -11),
    (-7.1, -8),
]

G = [
    None,
    (-6, -8),
    (-3, -5),
    (3, -5),
    (6, -8),
    (6, -14),
    (3, -17),
    (-3, -17),
    (-6, -14),
    (-1, -12),
    (0, -12),
    (0, -13),
    (-1, -13),
]

H = [
    None,
    (-8, 28),
    (-6, 27),
    (-4, 21),
    (-4, 20),
    (-5, 20),
    (-9, 25),
    (-9, 27),

    (6, 27),
    (8, 28),
    (9, 27),
    (9, 25),
    (5, 20),
    (4, 20),
    (4, 21),
]

def move(position):
    x, y = position
    goto(v(x), v(y))


def draw(positions):
    up()
    move(positions[0])
    down()
    for position in positions:
        move(position)
    up()


def v(value):
    return value * 10


def grid():
    for x in xrange(-14, 19):
        draw([(x, -20), (x, 30)])
    for y in xrange(-20, 31):
        draw([(-14, y), (18, y)])


def oreilles_haut_tete():
    draw(A[1:23])


def oeil_gauche():
    draw([A[22]] + B[1:8] + [A[22]])
    begin_fill()
    draw(B[8:12] + [B[8]])
    end_fill()


def oeil_droit():
    draw([A[1]] + B[12:18] + [B[18], A[1]])
    draw([B[22]] + B[19:22]+ [B[23], B[22], B[24]])


def bas_du_corps():
    draw([B[5]] + D[1:14])


def bouche():
    begin_fill()
    color('black', 'red')
    draw(C[1:17] + [C[1]])
    end_fill()
    color('black')


def dents():
    begin_fill()
    color('black', 'white')
    draw([C[17], C[18], C[19], C[2]])
    draw([C[21], C[22], C[23], C[20]])
    end_fill()
    color('black')


def bras_droit():
    draw([D[13]] + D[14:23])
    draw(D[23:29] + [B[14]])


def ventouse():
    draw(E[1:4] + [D[23]] + E[4:14] + [E[8]])
    draw(E[13:16] + [E[6]])
    draw([E[15], D[16]])
    draw([E[5], D[21]])
    draw([E[1], D[19]])


def bras_gauche():
    draw(F[1:9] + [F[7], F[10], F[11]])


def ventre_nombril():
    begin_fill()
    color('black', 'white')
    draw(G[1:9] + [G[1]])
    end_fill()
    begin_fill()
    draw(G[9:13] + [G[9]])
    end_fill()
    color('black')


def interieur_oreille_droite():
    draw(H[1:8] + [H[1]])


def interieur_oreille_gauche():
    draw(H[8:15] + [H[8]])


scene = Screen()
scene.setup(width=500, height=650, startx=0, starty=0)


speed("fastest")
width(1)
color('gray')
grid()

width(2)
color('black')
oreilles_haut_tete()
oeil_gauche()
oeil_droit()
bas_du_corps()
bouche()
dents()
bras_droit()
ventouse()
bras_gauche()
ventre_nombril()
interieur_oreille_droite()
interieur_oreille_gauche()

ht()
getcanvas().postscript(file="lapin.eps")
exitonclick()
