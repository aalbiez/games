import pygame
import time

blue = (22,12,234)
white = (255,255,255)

pygame.init()

surfaceW = 800
surfaceH = 500
ballonW = 50
ballonH = 66

surface = pygame.display.set_mode((surfaceW, surfaceH))
pygame.display.set_caption("Ballon Volant")
horloge = pygame.time.Clock()


def rejoue0uQuitte() :
    for event in pygame.pygame.event.get ([pygame.KEYDOWN, pygame.KEYUP, pygame.QUIT]):
        if event.type == pygame.QUIT :
            pygame.quit()
            quit()
        elif event.type == pygame.KEYUP :
            continue
        return event.KEY
    return None

img = pygame.image.load('ballon01.png')
def creaTexteObj(texte, police):
    texteSurface = Police.render(texte, True, whit)
    return texteSurface, texteSurface.get_rect()


def message(texte):
    GOTexte = pygame.font.Font('BraBunR.ttf', 150)
    petitTexte = pygame.font.Font('BraBunR.ttf', 20)

    GOTexteSurf, GOTexteRect = creaTexteObj(texte, GOTexte)
    GOTexteRect.centre = surfaceW/2, ((surfaceH/2)-50)
    surface.blit(GOtexteSurf, GOTexteRect)

    petitTexteSurf, petitTexteRect = creaTexteObj("appuyer sur une touche", petitTexte)
    petitTexteRect.centre = surfaceW/2, ((surfaceH/2)-50)
    surface.blit(petitTextexteSurf, petitTexteRect)

    pygame.display.update()
    time.sleep(2)

    while rejoueOuQuitte() == None :
        horloge.tick()

        principale()


def gameover():
    message("boom!")

def ballon(x,y,image):
    surface.blit(image, (x,y))

def principale():
    x = 150
    y = 200
    y_mouvement = 0

    game_over = False

    while not game_over :
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True

            if event.type == pygame.KEYDOWN :
                if event.key == pygame.K_UP :
                    y_mouvement = -5
                if event.type == pygame.KEYUP :
                    y_mouvement = 5

            y+= y_mouvement

        surface.fill(blue)
        ballon(x,y,img)

        if y >surfaceH -40 or y < -10 :
            gameover

        pygame.display.update()

principale()
pygame.quit()
quit()
